import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the TimeSelectionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-time-selection',
	templateUrl: 'time-selection.html'
})
export class TimeSelectionPage {
	constructor(
		public viewCtrl: ViewController,
		public navCtrl: NavController,
		public navParams: NavParams
	) {}

	ionViewDidLoad() {
		console.log('ionViewDidLoad TimeSelectionPage');
	}

	public closeModal() {
		this.viewCtrl.dismiss();
	}
}
