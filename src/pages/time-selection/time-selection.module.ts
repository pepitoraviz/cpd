import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimeSelectionPage } from './time-selection';

@NgModule({
  declarations: [
    TimeSelectionPage,
  ],
  imports: [
    IonicPageModule.forChild(TimeSelectionPage),
  ],
})
export class TimeSelectionPageModule {}
