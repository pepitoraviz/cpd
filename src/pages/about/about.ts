import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { TimeSelectionPage } from '../time-selection/time-selection';

@Component({
	selector: 'page-about',
	templateUrl: 'about.html'
})
export class AboutPage {
	constructor(public navCtrl: NavController, public modalCtrl: ModalController) {}

	presentModal() {
		let modalPage = this.modalCtrl.create(TimeSelectionPage);
		modalPage.present();
	}
}
