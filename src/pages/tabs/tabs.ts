import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { InvoicesPage } from '../invoices/invoices';
import { ProfilePage } from '../profile/profile';

@Component({
	templateUrl: 'tabs.html'
})
export class TabsPage {
	tab1Root = HomePage;
	tab2Root = AboutPage;
	tab3Root = ContactPage;
	tab4Root = InvoicesPage;
	tab5Root = ProfilePage;

	constructor() {}
}
